package me.prakashayappan.simplilearn.aws.webservice.integration.database.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import me.prakashayappan.simplilearn.aws.webservice.integration.database.entity.EmployeeInputTable;
import me.prakashayappan.simplilearn.aws.webservice.integration.database.repository.EmployeeInputRepository;

@Component
public class EmployeeInputDao {

	@Autowired
	private EmployeeInputRepository employeeInputRepository;

	public List<EmployeeInputTable> selectByUserName(String userName) {
		return employeeInputRepository.findByUserName(userName);
	}

	public EmployeeInputTable insert(EmployeeInputTable employeeInputTable) {
		return employeeInputRepository.save(employeeInputTable);
	}

	public Iterable<EmployeeInputTable> selectAll() {
		return employeeInputRepository.findAll();
	}

}