package me.prakashayappan.simplilearn.aws.webservice.service.model.carrier;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EmployeeInputGetModel {

	private String featureId;

	private Integer rating;

	private String comments;

}
