package me.prakashayappan.simplilearn.aws.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssociateProject1Service {

	public static void main(String[] args) {
		SpringApplication.run(AssociateProject1Service.class, args);
	}

}
