package me.prakashayappan.simplilearn.aws.webservice.service.model.carrier;

import lombok.Data;

@Data
public class EmployeeInputListModel {

	private String userName;

	private String featureId;

	private Integer rating;

	private String comments;

}
