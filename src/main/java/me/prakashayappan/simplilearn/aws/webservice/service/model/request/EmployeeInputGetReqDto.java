package me.prakashayappan.simplilearn.aws.webservice.service.model.request;

import lombok.Data;

@Data
public class EmployeeInputGetReqDto {

	private String featureId;

	private Integer rating;

	private String comments;

}