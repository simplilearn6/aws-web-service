package me.prakashayappan.simplilearn.aws.webservice.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import me.prakashayappan.simplilearn.aws.webservice.service.EmployeeInputService;
import me.prakashayappan.simplilearn.aws.webservice.service.model.request.EmployeeInputCreateReqDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputCreateResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputGetResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputListResDto;

@RestController
public class EmployeeInputEndpoint {

	@Autowired
	private EmployeeInputService employeeInputService;

	@PostMapping(path = "create", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeInputCreateResDto> postEmployeeInput(Authentication authentication,
			@RequestBody EmployeeInputCreateReqDto employeeInputCreateReqDto) {
		return new ResponseEntity<>(employeeInputService.createEmployeeInput((String) authentication.getPrincipal(), employeeInputCreateReqDto),
				HttpStatus.CREATED);
	}

	@GetMapping(path = "get", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeInputGetResDto> getEmployeeInput(Authentication authentication) {
		return ResponseEntity.ok(employeeInputService.readEmployeeInput((String) authentication.getPrincipal()));
	}

	@GetMapping(path = "list", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeInputListResDto> getEmployeeInputs() {
		return ResponseEntity.ok(employeeInputService.readEmployeeInputs());
	}

}