package me.prakashayappan.simplilearn.aws.webservice.service.model.request;

import lombok.Data;

@Data
public class EmployeeInputListReqDto {

	private String inputId;

}