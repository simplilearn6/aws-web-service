package me.prakashayappan.simplilearn.aws.webservice.service.helper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import me.prakashayappan.simplilearn.aws.webservice.integration.database.entity.EmployeeInputTable;
import me.prakashayappan.simplilearn.aws.webservice.service.model.carrier.EmployeeInputGetModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.carrier.EmployeeInputListModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.request.EmployeeInputCreateReqDto;

@Component
public class EmployeeInputHelper {

	public EmployeeInputTable getEmployeeInputTable(String userName, EmployeeInputCreateReqDto employeeInputCreateReqDto) {
		EmployeeInputTable employeeInputTable = new EmployeeInputTable();
		employeeInputTable.setUserName(userName);
		employeeInputTable.setFeatureId(employeeInputCreateReqDto.getFeatureId());
		employeeInputTable.setRating(employeeInputCreateReqDto.getRating());
		employeeInputTable.setComments(employeeInputCreateReqDto.getComments());
		employeeInputTable.setCreatedAt(new Date());
		return employeeInputTable;
	}

	public List<EmployeeInputGetModel> doTransform(List<EmployeeInputTable> employeeInputTables) {
		List<EmployeeInputGetModel> employeeInputGetModels = new ArrayList<>();
		Optional.ofNullable(employeeInputTables).orElse(new ArrayList<>()).parallelStream().forEach(e -> {
			EmployeeInputGetModel employeeInputGetModel = new EmployeeInputGetModel(e.getFeatureId(), e.getRating(), e.getComments());
			employeeInputGetModels.add(employeeInputGetModel);
		});
		return employeeInputGetModels;
	}

	public List<EmployeeInputListModel> doTransform(Iterable<EmployeeInputTable> employeeInputTables) {
		List<EmployeeInputListModel> employeeInputListModels = new ArrayList<>();
		employeeInputTables.spliterator().forEachRemaining(e -> {
			EmployeeInputListModel employeeInputListModel = new EmployeeInputListModel();
			employeeInputListModel.setUserName(e.getUserName());
			employeeInputListModel.setFeatureId(e.getFeatureId());
			employeeInputListModel.setRating(e.getRating());
			employeeInputListModel.setComments(e.getComments());
			employeeInputListModels.add(employeeInputListModel);
		});
		return employeeInputListModels;
	}

}
