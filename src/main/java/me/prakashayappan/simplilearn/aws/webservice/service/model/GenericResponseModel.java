package me.prakashayappan.simplilearn.aws.webservice.service.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResponseModel {

	private boolean success;

	private String responseCode;

	private String errorDescription;

}