package me.prakashayappan.simplilearn.aws.webservice.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import me.prakashayappan.simplilearn.aws.webservice.service.exception.BadRequestException;
import me.prakashayappan.simplilearn.aws.webservice.service.model.GenericResponseModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.GenericResponseModel.GenericResponseModelBuilder;

@RestControllerAdvice
public class EndpointHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<GenericResponseModel> handleGenericException(Exception exception) {
		return new ResponseEntity<>(getGenericResponseModel("INTERNAL_SERVER_ERROR", "Something went Wrong"), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<GenericResponseModel> handleBadRequestException(BadRequestException badRequestException) {
		return new ResponseEntity<>(getGenericResponseModel("BAD_REQUEST", "Unexpected Input from Client"), HttpStatus.BAD_REQUEST);
	}

	private GenericResponseModel getGenericResponseModel(String responseCode, String errorDescription) {
		GenericResponseModelBuilder<?, ?> response = GenericResponseModel.builder();
		response.success(false);
		response.responseCode(responseCode);
		response.errorDescription(errorDescription);
		return response.build();
	}

}