package me.prakashayappan.simplilearn.aws.webservice.views;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.slf4j.Slf4j;
import me.prakashayappan.simplilearn.aws.webservice.service.EmployeeInputService;
import me.prakashayappan.simplilearn.aws.webservice.service.model.request.EmployeeInputCreateReqDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputGetResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputListResDto;

@Controller
@Slf4j
public class ViewController {

	@Autowired
	private EmployeeInputService employeeInputService;

	@GetMapping(path = { "/", "/home", "/index" })
	public String homeView(Model model) {
		model.addAttribute("name", "One");
		return "HomeTemplate";
	}

	@GetMapping(path = { "/input/submit" })
	public String submitInputView(Model model) {
		model.addAttribute("greeting", new EmployeeInputCreateReqDto());
		log.info("Get Submission Called");
		return "SubmitInputTemplate";
	}

	@PostMapping(path = { "/input/submit" })
	public String submitInputView(Authentication authentication, @ModelAttribute EmployeeInputCreateReqDto greeting, Model model) {
		log.info("Post Submission Called" + greeting.getFeatureId());
		User user = (User) authentication.getPrincipal();
		employeeInputService.createEmployeeInput(user.getUsername(), greeting);
		model.addAttribute("greeting", new EmployeeInputCreateReqDto());
		model.addAttribute("formResponse", "Your Input Added!");
		return "SubmitInputTemplate";
	}

	@GetMapping(path = { "/myinputs" })
	public String myInputView(Authentication authentication, Model model) {
		User user = (User) authentication.getPrincipal();
		EmployeeInputGetResDto response = employeeInputService.readEmployeeInput(user.getUsername());
		model.addAttribute("inputs", response.getEmployeeInputGetModels());
		return "MyInputsTemplate";
	}

	@GetMapping(path = { "/allinputs" })
	public String allInputView(Model model) {
		EmployeeInputListResDto response = employeeInputService.readEmployeeInputs();
		model.addAttribute("inputs", response.getEmployeeInputListModels());
		return "AllInputsTemplate";
	}

	@GetMapping(value = "/logout")
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "HomeTemplate";
	}

	@GetMapping(path = { "/error" })
	public String error(Model model) {
		return "ErrorTemplate";
	}

}