package me.prakashayappan.simplilearn.aws.webservice.service.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import me.prakashayappan.simplilearn.aws.webservice.service.model.GenericResponseModel;

@Data
@EqualsAndHashCode(callSuper = false)
@SuperBuilder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeInputCreateResDto extends GenericResponseModel {

	private Long inputId;

}