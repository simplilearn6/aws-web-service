package me.prakashayappan.simplilearn.aws.webservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.prakashayappan.simplilearn.aws.webservice.integration.database.dao.EmployeeInputDao;
import me.prakashayappan.simplilearn.aws.webservice.integration.database.entity.EmployeeInputTable;
import me.prakashayappan.simplilearn.aws.webservice.service.helper.EmployeeInputHelper;
import me.prakashayappan.simplilearn.aws.webservice.service.model.carrier.EmployeeInputGetModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.carrier.EmployeeInputListModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.request.EmployeeInputCreateReqDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputCreateResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputCreateResDto.EmployeeInputCreateResDtoBuilder;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputGetResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputGetResDto.EmployeeInputGetResDtoBuilder;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputListResDto;
import me.prakashayappan.simplilearn.aws.webservice.service.model.response.EmployeeInputListResDto.EmployeeInputListResDtoBuilder;

@Service
public class EmployeeInputService {

	@Autowired
	private EmployeeInputDao employeeInputDao;

	@Autowired
	private EmployeeInputHelper employeeInputHelper;

	public EmployeeInputGetResDto readEmployeeInput(String userName) {
		EmployeeInputGetResDtoBuilder<?, ?> response = EmployeeInputGetResDto.builder();
		List<EmployeeInputTable> employeeInputTables = employeeInputDao.selectByUserName(userName);
		List<EmployeeInputGetModel> employeeInputGetModels = employeeInputHelper.doTransform(employeeInputTables);
		response.employeeInputGetModels(employeeInputGetModels);
		return response.success(Boolean.TRUE).build();
	}

	public EmployeeInputCreateResDto createEmployeeInput(String userName, EmployeeInputCreateReqDto employeeInputCreateReqDto) {
		EmployeeInputCreateResDtoBuilder<?, ?> response = EmployeeInputCreateResDto.builder();
		EmployeeInputTable employeeInputTable = employeeInputHelper.getEmployeeInputTable(userName, employeeInputCreateReqDto);
		EmployeeInputTable savedEmployeeInputTable = employeeInputDao.insert(employeeInputTable);
		response.inputId(savedEmployeeInputTable.getId());
		return response.success(Boolean.TRUE).build();
	}

	public EmployeeInputListResDto readEmployeeInputs() {
		EmployeeInputListResDtoBuilder<?, ?> response = EmployeeInputListResDto.builder();
		List<EmployeeInputListModel> employeeInputListModels = employeeInputHelper.doTransform(employeeInputDao.selectAll());
		response.employeeInputListModels(employeeInputListModels);
		return response.success(Boolean.TRUE).build();
	}

}