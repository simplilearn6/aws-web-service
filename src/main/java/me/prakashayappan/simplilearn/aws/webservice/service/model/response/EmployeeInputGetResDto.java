package me.prakashayappan.simplilearn.aws.webservice.service.model.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import me.prakashayappan.simplilearn.aws.webservice.service.model.GenericResponseModel;
import me.prakashayappan.simplilearn.aws.webservice.service.model.carrier.EmployeeInputGetModel;

@Data
@EqualsAndHashCode(callSuper = false)
@SuperBuilder(toBuilder = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeInputGetResDto extends GenericResponseModel {

	private List<EmployeeInputGetModel> employeeInputGetModels;

}