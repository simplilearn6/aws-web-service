package me.prakashayappan.simplilearn.aws.webservice.integration.database.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "employee_input")
@Getter
@Setter
public class EmployeeInputTable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String userName;

	private String featureId;

	private Integer rating;

	private String comments;

	private Date createdAt;

}