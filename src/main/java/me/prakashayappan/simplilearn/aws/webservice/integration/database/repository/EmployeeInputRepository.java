package me.prakashayappan.simplilearn.aws.webservice.integration.database.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.prakashayappan.simplilearn.aws.webservice.integration.database.entity.EmployeeInputTable;

@Repository
public interface EmployeeInputRepository extends CrudRepository<EmployeeInputTable, Long> {

	public List<EmployeeInputTable> findByUserName(String userName);

}