package me.prakashayappan.simplilearn.aws.webservice.integration.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	@Bean
	public UserDetailsService userDetailsService() {
		List<UserDetails> userDetailsList = new ArrayList<>();
		userDetailsList.add(User.withUsername("admin").password(getPasswordEncoder().encode("password")).roles("ADMIN", "MANAGER", "USER").build());
		userDetailsList.add(User.withUsername("manager").password(getPasswordEncoder().encode("password")).roles("MANAGER").build());
		userDetailsList.add(User.withUsername("user1").password(getPasswordEncoder().encode("password")).roles("USER").build());
		return new InMemoryUserDetailsManager(userDetailsList);
	}

	@Bean
	public PasswordEncoder getPasswordEncoder() {

		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/home", "/about", "/health").permitAll().antMatchers("/input/submit", "/myinputs").hasRole("USER")
				.antMatchers("/allinputs").hasRole("MANAGER").anyRequest().authenticated().and().formLogin().and()
				.logout(e -> e.logoutUrl("/logout").logoutSuccessUrl("/")).exceptionHandling(e -> e.accessDeniedPage("/error"));
	}

}
