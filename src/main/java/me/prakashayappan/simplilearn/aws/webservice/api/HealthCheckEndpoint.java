package me.prakashayappan.simplilearn.aws.webservice.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckEndpoint {

	@GetMapping(path = "/health")
	public ResponseEntity<String> getEmployeeInput() {
		return ResponseEntity.ok("OK");
	}

}